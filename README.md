# OpenML dataset: Goodreads-Books---31-Features

https://www.openml.org/d/43438

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The official Goodread's API limits retrievable data, so I decided to scrape the actual HTTP pages and grab additional details on each book.
Content
Books are scraped from a list titles the "Best Books Ever" which can be found here https://www.goodreads.com/list/show/1.Best_Books_Ever
Acknowledgements
Thanks to Goodreads for housing the data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43438) of an [OpenML dataset](https://www.openml.org/d/43438). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43438/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43438/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43438/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

